#!/usr/bin/env python
# need to change working dir for import to work, honestly don't know why
import os
os.chdir('/home/mgaber/Workbench/thinkstats/work')

import thinkstats2
import thinkplot
import nsfg
import pandas as pd
import numpy as np

# Load the data
# default function assumes files are on the same path, need absolute paths
df = nsfg.ReadFemPreg(dct_file='/home/mgaber/Workbench/thinkstats/ThinkStats2/code/2002FemPreg.dct', dat_file='/home/mgaber/Workbench/thinkstats/ThinkStats2/code/2002FemPreg.dat.gz')

df.columns
df.head()
df.shape

nsfg.CleanFemPreg(df)
df.head()

# df = CleanFemPreg(df)
# df[['totalwgt_lb', 'agepreg', 'birthwgt_lb', 'birthwgt_oz']]
# df['birthwgt_oz'].unique()
# df['birthwgt_lb'].unique()

df.describe()
df.outcome.value_counts().sort_index()
df.birthwgt_lb.value_counts().sort_index()
caseid = 10229
preg_map = nsfg.MakePregMap(df)
type(preg_map)
indices = preg_map[caseid]
df.outcome[indices].values
