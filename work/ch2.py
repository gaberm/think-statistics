#!/usr/bin/env python
# need to change working dir for import to work, honestly don't know why
import os
os.chdir('/home/mgaber/Workbench/thinkstats/work')

import inspect
import pandas as pd
import numpy as np

import thinkstats2
import thinkplot
import nsfg

# Load the data
# default function assumes files are on the same path, need absolute paths
data_path = '/home/mgaber/Workbench/thinkstats/ThinkStats2/code'
outcome_label = ['Live Birth', 'Induced Abortion', 'StillBirth', 'Miscarriage', 'Ectopic pregnancy', 'Current pregnancy']

'''
hist = {}
t = [0, 1 ,2 ,3 ,2, 1,2 ,1 , 1,3, 0]
for x in t:
    hist[x] = hist.get(x, 0) + 1

# or
from collections import Counter
counter = Counter(t)
'''
# Loading Data
# Preg
preg = nsfg.ReadFemPreg(f'{data_path}/2002FemPreg.dct', dat_file=f'{data_path}/2002FemPreg.dat.gz')
# Female respondents
resp = nsfg.ReadFemResp(dct_file=f'{data_path}/2002FemResp.dct', dat_file=f'{data_path}/2002FemResp.dat.gz')

hist = thinkstats2.Hist(live.birthwgt_lb, label='birthwgt_lb')
thinkplot.Hist(hist)
thinkplot.Show(xlabel='pounds', ylabel='frequenncy')

hist = thinkstats2.Hist(live.agepreg, label='agepreg')
thinkplot.Hist(hist)

hist = thinkstats2.Hist(live.prglngth, label='preg length')
thinkplot.Hist(hist)

# Outliers
for weeks, freq in hist.Smallest(10):
    print(weeks, freq)
for weeks, freq in hist.Largest(10):
    print(weeks, freq)
# first babies
live = preg[preg.outcome == 1]
firsts = live[live.birthord == 1]
others = live[live.birthord != 1]

first_hist = thinkstats2.Hist(firsts.prglngth, label='first')
other_hist = thinkstats2.Hist(others.prglngth, label='other')

width=0.45
thinkplot.PrePlot(2)
thinkplot.Hist(first_hist, align='right', width=width)
thinkplot.Hist(other_hist, align='left', width=width)
thinkplot.Show(xlabel='weeks', ylabel='freq', xlim=[27,46])
## Exercies
 def Mode(histo):
    mfrequent = first_hist.Largest()[-1]
    (i,j) = mfrequent
    return i

Mode(first_hist)

# Exercise book
hist = thinkstats2.Hist(live.birthwgt_lb, label='birthwgt_lb')
thinkplot.Hist(hist)
thinkplot.Config(xlabel='Birth weight (pounds)', ylabel='Count')
## age - no floor
hist = thinkstats2.Hist(live.agepreg, label='agepreg')
thinkplot.Hist(hist, label='agepreg')

ages = np.floor(live.agepreg)
hist = thinkstats2.Hist(ages, label='agepreg')
thinkplot.Hist(hist, label='agepreg')
thinkplot.Hist(hist)
help(np.floor)
np.info(np.floor)

## histo of preg length
first_floored = np.floor(live.prglngth)
hist = thinkstats2.Hist(first_floored, label='preg length')
thinkplot.Hist(first_hist, align='right', width=width)

for i, j in hist.Largest():
    print(i, j)

def CohenEffectSize(group1, group2):

    diff = group1.mean() - group2.mean()
    var1 = group1.var()
    var2 = group2.var()
    n1, n2 = len(group1), len(group2)

    pooled_var = (n1 * var1 + n2 * var2) / (n1 - n2)
    pooled_var = abs(pooled_var)

    d = diff / np.sqrt(pooled_var)
    return d

CohenEffectSize(firsts.prglngth.values, others.prglngth.values)

## Exercises
import matplotlib.pyplot as plt

plt.hist(first_floored)
plt.hist( ages)

width = 0.45
thinkplot.PrePlot(2)
thinkplot.Hist(first_hist, align='right', width=width)
thinkplot.Hist(other_hist, align='left', width=width)
thinkplot.Config(xlabel='weeks', ylabel='Count', xlim=[27, 46])

mean= live.prglngth.mean()
var = live.prglngth.var()
std = live.prglngth.std()
std == np.sqrt(var)

CohenEffectSize(firsts.totalwgt_lb, others.totalwgt_lb)

## total weight
live.totalwgt_lb.mean()
firsts.totalwgt_lb.mean()
others.totalwgt_lb.mean()

first_hist = thinkstats2.Hist(firsts.totalwgt_lb, label='first')
other_hist = thinkstats2.Hist(others.totalwgt_lb, label='other')

width=0.45
thinkplot.PrePlot(2)
thinkplot.Hist(first_hist, align='right', width=width)
thinkplot.Hist(other_hist, align='left', width=width)
thinkplot.Show(xlabel='totalwgt_lb', ylabel='freq')
