# On Statistics
* Wikipedia
* www.reddit.com/r/statistics

# Book repo
https://github.com/AllenDowney/ThinkStats2

# Statsistics foundation
Statistics with R" track offered by the Duke University on the Coursera platform. This track is consisted of 5 courses that you can audit for free.

# Online Jupyters
https://jupyter.org/try
